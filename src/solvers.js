window.findNRooksSolution = function(n) {
  var board = new Board({n:n});
  var solution = [];
  var matrix = board.rows();
  for(var i = 0; i < matrix.length; i++){
    for(var j = 0; j < matrix[i].length; j++){
        board.togglePiece(i, j);
      if(board.hasAnyRooksConflicts()){
        board.togglePiece(i, j);
      }
    }
  }

  solution = matrix;

  console.log('Single solution for ' + n + ' rooks:', JSON.stringify(solution));
  return solution;
};

// return the number of nxn chessboards that exist, with n rooks placed such that none of them can attack each other
window.countNRooksSolutions = function(n) {
  var board = new Board({n:n});
  var solutionCount = 0;
  var matrix = board.rows();
  //toggle, check, recursion, untoggle
  var row = 0;

  var innerCheck = function(row){

    if(row === n){
      solutionCount++;
      return;
    }

    for(var i = 0; i < matrix[row].length; i++){
      board.togglePiece(row, i);
      if(!board.hasAnyRooksConflicts()){
        innerCheck(row+1);
      }
      board.togglePiece(row, i)
    }
  };

  innerCheck(row);

  // for (var i = 0; i < matrix.length; i++) {
  //   for (var j = 0; j < matrix.length; j++) {
      // var innerHelper = function(rowIndex, columnIndex, countPiece, board) {
      //   // rowIndex = i;
      //   // columnIndex = j;
      //   board.togglePiece(rowIndex, columnIndex); //place a piece

      //   if (board.hasAnyRooksConflicts()) { //if placed piece has conflicts =>
      //     board.togglePiece(rowIndex, columnIndex); //remove piece
      //     columnIndex++; // move one square to the right

      //     if (columnIndex < n) { // when at last square in row
      //       rowIndex++;
      //       columnIndex = 0; // move to next row
      //     }

      //     if(columnIndex < n && rowIndex < n){ // check if at the last square, if not
      //       innerHelper(rowIndex, columnIndex, countPiece, board); //
      //     }
      //   } else {

      //     // while(columnIndex < n && rowIndex < n){
      //     //   board.togglePiece(rowIndex, columnIndex);
      //     //   columnIndex++;
      //     //   if(columnIndex === n){
      //     //     rowIndex++;
      //     //   }
      //     //   innerHelper(rowIndex, columnIndex, countPiece, board);
      //     // }

      //     countPiece++;
      //     if (countPiece === n) {
      //       solutionCount++;
      //     }

      //     if(columnIndex < n){
      //       rowIndex++;
      //       columnIndex = 0;
      //     }

      //     if(columnIndex < n && rowIndex < n){
      //       innerHelper(rowIndex, columnIndex, countPiece, board);
      //    }
          // if(columnIndex <= n && rowIndex <= n){
          //   board.togglePiece(rowIndex, columnIndex);
          //   innerHelper(rowIndex, columnIndex, countPiece, board);
          // }
      //  }

      //};

      //  innerHelper(0,0,countPiece,board);

  //   }
  // }

  console.log('Number of solutions for ' + n + ' rooks:', solutionCount);
  return solutionCount;
};

// return a matrix (an array of arrays) representing a single nxn chessboard, with n queens placed such that none of them can attack each other
window.findNQueensSolution = function(n) {


  var board = new Board({n:n});
  var solution = undefined;

  if(n === 2 || n === 3){
    return {n: n};
  }

  var innerSolution = function(row){

    row = row || 0;

    if(row === n){
      solution = [];

      for(var i = 0; i < n; i++){
        solution.push(board.rows()[i].slice());
      }
      return;
    }

    for(var i = 0; i < n; i++){
      board.togglePiece(row, i);
      if(!board.hasAnyQueensConflicts()){

        innerSolution(row + 1);
      }

      board.togglePiece(row, i);
    }
  };

  innerSolution();

  console.log('Single solution for ' + n + ' queens:', JSON.stringify(solution));
  return solution;
};

// return the number of nxn chessboards that exist, with n queens placed such that none of them can attack each other
window.countNQueensSolutions = function(n) {
  var solutionCount = undefined; //fixme

  var board = new Board({n:n});
  var solutionCount = 0;
  var matrix = board.rows();
  var row = 0;

  var innerCheck = function(row){

    if(row === n){
      solutionCount++;
      return;
    }

    for(var i = 0; i < matrix[row].length; i++){
      board.togglePiece(row, i);
      if(!board.hasAnyQueensConflicts()){
        innerCheck(row+1);
      }
      board.togglePiece(row, i)
    }
  };

  innerCheck(row);

  console.log('Number of solutions for ' + n + ' queens:', solutionCount);
  return solutionCount;
};
